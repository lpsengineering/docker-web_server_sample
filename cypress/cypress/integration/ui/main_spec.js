// eslint-ignore-file
// type definitions for Cypress object "cy"
// <reference types="cypress" />

// type definitions for custom commands like "createDefaultTodos"
// <reference types="../support" />

// check this file using TypeScript if available
// @ts-check

// ***********************************************
// All of these tests are written to implement
// the official TodoMVC tests written for Selenium.
//
// The Cypress tests cover the exact same functionality,
// and match the same test names as TodoMVC.
// Please read our getting started guide
// https://on.cypress.io/introduction-to-cypress
//
// You can find the original TodoMVC tests here:
// https://github.com/tastejs/todomvc/blob/master/tests/test.js
// ***********************************************

describe('[UI] Homepage', () => {
	// setup these constants to match what TodoMVC does
	beforeEach(() => {
		cy.visit('/')
	})

	context('When home page is opened', () => {
		it('should navigate through all tabs', () => {
			cy.get('#introTab').click()
			cy.visit('/')
			cy.get('#workTab').click()
			cy.visit('/')
			cy.get('#aboutTab').click()
			cy.visit('/')
			cy.get('#contactTab').click()
			cy.visit('/')
		})
	})
})
