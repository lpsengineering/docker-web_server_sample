# Docker Workbench

This project is a compilation of projects used in our Docker training last April 24 - May 1.

### Day 2 - Static web server using Express Node.js

This is a web server built on top of an Express Docker image which copies all of the files inside the directory and onboards it on the Docker image's `/public` directory to run it on the webserver.

##### Commands
 - `docker build .` = builds the docker image located on the location supplied which is `.` which means the current directory.
 - `docker load -d -p 3000:3000 <image id>` = loads built image into a container and mounts it on the provided port number.

##### Files
- `Dockerfile` = defines how Docker images are built
- `index.html` = Static webpage
- `/assets` = CSS, fonts, js and other files used by the webpage
- `images` = Images used by the static webpage
- `error` = Error 404 webpage

### Day 3 - Container and Image management + Nginx web server

Demonstrated how docker containers are lifecycle managed via command line as well as another example using the same web server built on top of an *nginx* Docker image to allow users to customize the web pages while it is running by using `docker exec`. Also included is a tutorial on how to save and load built docker images for re-use and distribution.

##### Commands

 - `docker exec <container id> <interface>` = attaches the user inside the shell of the Docker container and provides an interface between on the provided interface parameter.
 - `docker ps` = lists all running containers.
 - `docker inspect <container id>` = shows container status, port ,etc.
 - `docker kill` = kills the server by sending a `SIGINT` command, preventing a "graceful shutdown".
 - `docker system prune` = removes all "dangling" images, providing an `-a` parameter will cause system prune to also delete mounted images on stopped containers.
 - `docker save <container id>` = saves the container into an image including all changes performed inside it.
 - `docker load <file>` = loads a Docker image tarball into the user's image repository.

##### Files

 - `/nginx` = contains all files including the Dockerfile which will be put into the nginx server's `/www` folder.


### Day 5 - Mounting a Webserver + DB + Administrator all at once using Docker Compose, Environment management and Docker security basics.

### Commands
 - `docker-compose up -d` = compiles, builds and loads all of the images and configs specified on the docker-compose file.
 - `.env` = Environment variables

### Files

- `/wordpress-compose` = contains all files required by Docker compose to mount a simple Wordpress server with MySQL and PHPMyAdmin.