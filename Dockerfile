FROM duluca/minimal-node-web-server

WORKDIR /usr/src/app

ENV NODE_ENV=local

COPY assets public/assets
COPY error public/error
COPY images public/images
COPY index.html public